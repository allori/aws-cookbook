# Set up app's custom configuration in the environment.
# See https://forums.aws.amazon.com/thread.jspa?threadID=118107

include_recipe "rails::configure"

node[:deploy].each do |application, deploy|
  
  dotenv do
    application application
    deploy deploy
    env node[:env]
  end
  
end
