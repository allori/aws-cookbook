# opsworks_dotenv

A Chef Recipe based on [opsworks_custom_env](https://github.com/joeyAghion/opsworks_custom_env/).

This cookbook maintains a `.env` file in each respective app's deploy directory. E.g.:

    FOO=http://www.yahoo.com
    BAR=1001

Your application can then load its settings directly from this file, or use [Dotenv](https://github.com/bkeepers/dotenv) to automatically make these settings available in the app's `ENV` (recommended).

Expects attributes of the form:

    {
      "env": {
        "FOO": "http://www.yahoo.com",
        "my_app": {
          "BAR": "1001"
        },
        "your_app": {
          "BAZ": "99"
        }
      }
    }


Note that, at the moment, only Unicorn/Nginx-style Rails apps are supported.


Opsworks Set-Up
---------------

The `opsworks_dotenv::configure` recipe should be added as a custom recipe to the _Setup_, _Configure_, and _Deploy_ events.

A deploy is not necessary to update custom application environment values. Instead, update the Stack's custom JSON, then choose to _Run Command_ > _execute recipes_ and enter `opsworks_dotenv::update` into the _Recipes to execute_ field. Executing this recipe will write an updated `application.yml` file and restart the unicorn workers.
