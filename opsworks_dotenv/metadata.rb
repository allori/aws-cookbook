name "opsworks_dotenv"
maintainer "Dave Burt"
maintainer_email "dave.burt@allori.edu.au"
description "Writes a .env file with custom ENV values to apps' deploy directories."
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version "0.1.3"

recipe "opsworks_dotenv::configure", "Write a .env file to app's Rails root. Relies on restart command declared by rails::configure recipe. (Intended as part of configure/deploy OpsWorks events.)"
recipe "opsworks_dotenv::update", "Write an updated .env and restart the app. Can be run independently of OpsWorks configure/deploy events."

# This actually depends on the rails::configure recipe by OpsWorks, but not
# declaring that here to prevent librarian-chef failure.
# depends "rails::configure"
